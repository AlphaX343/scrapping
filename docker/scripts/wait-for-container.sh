#!/bin/sh
until nc -vz $1 $2; do echo "Waiting for container $1:$2..."; sleep 3; done;