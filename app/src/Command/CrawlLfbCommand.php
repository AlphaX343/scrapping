<?php

namespace App\Command;

use App\Service\LfbCrawler;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'crawl:lfb',
    description: 'Add a short description for your command',
)]
class CrawlLfbCommand extends Command
{
    public function __construct(private readonly LfbCrawler $crawler)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $errors = $this->crawler->crawl();
        if (!empty($errors)) {
            foreach ($errors as $error) {
                $output->writeln($error);
            }

            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
