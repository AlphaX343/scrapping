<?php

namespace App\Domain\LfbCrawler\Contrat;

interface HistoriqueInterface
{
    /**
     * @return array<mixed>
     */
    public function getHistorique(): array;
}