<?php

namespace App\Domain\LfbCrawler\Contrat;

use GuzzleHttp\Exception\GuzzleException;

interface CrawlSiteInterface
{
    /**
     * @return array<string>
     * @throws RuntimeException
     */
    public function crawl(string $division): array;

    /**
     * @return array<string, string>
     * @throws GuzzleException
     */
    public function crawlDivisions(): array;
}