<?php

namespace App\Form;

use App\Domain\LfbCrawler\Contrat\CrawlSiteInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DivisionType extends AbstractType
{
    public function __construct(private readonly CrawlSiteInterface $crawler)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $divsions = $this->crawler->crawlDivisions();
        $builder
            ->add('division', ChoiceType::class, [
                'choices' => $divsions,
                'label' => 'Divisions disponibles'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Charger',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }

}
