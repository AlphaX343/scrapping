<?php

namespace App\Controller;

use App\Domain\LfbCrawler\Contrat\CrawlSiteInterface;
use App\Domain\LfbCrawler\Contrat\HistoriqueInterface;
use App\Form\DivisionType;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class HomeController extends AbstractController
{
    public function __construct(private readonly CrawlSiteInterface $crawler, private readonly HistoriqueInterface $historique)
    {
    }

    #[Route('/', name: 'app_home')]
    public function index(Request $request): Response
    {
        $form = $this->createForm(DivisionType::class);
        $form->handleRequest($request);
        $files = [];
        $errors = null;

        if ($form->isSubmitted() && $form->isValid()) {
            $divisionData = $form->getData();
            try {
                $files = $this->crawler->crawl($divisionData['division']);
            } catch (\RuntimeException $e) {
                $errors = 'aucuns matchs joués';
            }
        }

        $historique = $this->historique->getHistorique();

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'form' => $form->createView(),
            'files' => $files,
            'errors' => $errors,
            'historiqueDivisions' => $historique,
        ]);
    }
}
