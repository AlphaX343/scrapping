<?php

namespace App\Service;

use App\Domain\LfbCrawler\Contrat\CrawlSiteInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DomCrawler\Crawler;

readonly class LfbCrawler implements CrawlSiteInterface
{
    public function __construct(private string $urlLfb)
    {
    }

    /**
     * @return array<string>
     * @throws RuntimeException
     */
    public function crawl(string $division = 'LF2'): array
    {
        $client = new Client();
        $request = $client->request('GET', $this->urlLfb);

        $crawler = new Crawler();
        $crawler->addHtmlContent(str_replace("\t", "", $request->getBody()->getContents()));
        $divisionSelector = sprintf(
            '#%s a',
            $division
        );

        $ids = $crawler->filter($divisionSelector)->each(function (Crawler $node, $i) {
            return (int)filter_var($node->attr('href'), FILTER_SANITIZE_NUMBER_INT);
        });

        $outputDir = sprintf(
            '%s/../../public/output/%s',
            __DIR__,
            $division
        );
        if (!is_dir($outputDir)) {
            mkdir($outputDir);
        }

        $files = [];
        foreach ($ids as $id) {
            $filepath = sprintf('%s/%s.json', $outputDir, $id);
            if (file_exists($filepath)) {
                unlink($filepath);
            }

            $files[$id] = [
                'filepath' => $filepath,
                'name' => sprintf('%s/%s.json', $division, $id),
            ];

            try {
                $request = $client->request(
                    'GET',
                    sprintf('https://fibalivestats.dcd.shared.geniussports.com/data/%d/data.json', $id)
                );
                file_put_contents($filepath, json_encode($request->getBody()->getContents(), JSON_PRETTY_PRINT));
            } catch (\Exception|GuzzleException $e) {
                throw new \RuntimeException(
                    sprintf(
                        '<error>ID %d non joué, erreur : %s</error>',
                        $id,
                        $e->getMessage()
                    )
                );
            }
        }

        return $files;
    }

    /**
     * @return array<string, string>
     * @throws GuzzleException
     */
    public function crawlDivisions(): array
    {
        $client = new Client();
        $request = $client->request('GET', $this->urlLfb);

        $crawler = new Crawler();
        $crawler->addHtmlContent(str_replace("\t", "", $request->getBody()->getContents()));

        $divisions = $crawler->filter('.nav.nav-tabs> li')->each(function (Crawler $node, $i) {
            return trim($node->text());
        });

        $toReturn = [];
        foreach ($divisions as $division) {
            $toReturn[$division] = $division;
        }

        return $toReturn;
    }

}