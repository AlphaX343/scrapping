<?php

namespace App\Service;

use App\Domain\LfbCrawler\Contrat\HistoriqueInterface;
use Symfony\Component\Finder\Finder;

class LfbHistorique implements HistoriqueInterface
{
    private Finder $finder;

    public function __construct(private readonly string $lfbDirectory)
    {
        $this->finder = new Finder();
    }

    /**
     * @inheritDoc
     */
    public function getHistorique(): array
    {
        $files = [];
        $this->finder->files()->in($this->lfbDirectory);
        foreach ($this->finder as $file) {
            if ($file->isFile()) {
                $division = $file->getRelativePath();

                $date = new \DateTime();
                $date->setTimestamp($file->getMTime());

                $files[$division][] = [
                    'filename' => $file->getFilename(),
                    'path' => sprintf('output/%s', $file->getRelativePathname()),
                    'date' => $date,
                ];
            }
        }

        return $files;
    }
}